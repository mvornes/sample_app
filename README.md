# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).

---

## Chapter Exercises - Questions and Answers - Michael Vornes

#### Chapter 3
#### Section 3.1

1 - Confirm that Bitbucket renders the Markdown for the README in Listing 3.3 as HTML.

A: Yes, the Bitbucket renders the Markdown.

2 - By visiting the root route on the production server, verify that the deployment to Heroku succeeded.

A: Yes, the deployment to Heroku succeeded.

#### Section 3.2.1

1 - Generate a controller called Foo with actions bar and baz.

```
$ rails generate controller Foo bar baz
```

2 - By applying the techniques described in Box 3.1, destroy the Foo controller and its associated actions.

```
$ rails destroy controller Foo bar baz
```

#### Section 3.4.2

1 - You may have noticed some repetition in the Static Pages controller test (Listing 3.24). In particular, the base title, “Ruby on Rails Tutorial Sample App”, is the same for every title test. Using the special function setup, which is automatically run before every test, verify that the tests in Listing 3.30 are still green. (Listing 3.30 uses an instance variable, seen briefly in Section 2.2.2 and covered further in Section 4.4.5, combined with string interpolation, which is covered further in Section 4.2.2.)

A: Using the function setup with the base title the tests are still green.

#### Section 3.4.3

1 - Make a Contact page for the sample app.17 Following the model in Listing 3.15, first write a test for the existence of a page at the URL /static_pages/contact by testing for the title “Contact | Ruby on Rails Tutorial Sample App”. Get your test to pass by following the same steps as when making the About page in Section 3.3.3, including filling the Contact page with the content from Listing 3.40.

file: test/controllers/static_pages_controller_test.rb

    test "should get contact" do
      get static_pages_contact_url
      assert_response :success
      assert_select "title", "Contact | #{@base_title}"
    end

#### Section 3.4.4

1 - Adding the root route in Listing 3.41 leads to the creation of a Rails helper called root_url (in analogy with helpers like static_pages_home_url). By filling in the code marked FILL_IN in Listing 3.42, write a test for the root route.

file: test/controllers/static_pages_controller_test.rb

    test "should get root" do
      get root_url
      assert_response :success
      assert_select "title", "Home | #{@base_title}"
    end

2 - Due to the code in Listing 3.41, the test in the previous exercise is already green. In such a case, it’s harder to be confident that we’re actually testing what we think we’re testing, so modify the code in Listing 3.41 by commenting out the root route to get to red (Listing 3.43). (We’ll talk more about Ruby comments in Section 4.2.1.) Then uncomment it (thereby restoring the original Listing 3.41) and verify that you get back to green.

A: Commenting the line of root route you can get RED. Uncommenting returns to GREEN.


#### Chapter 4
#### Section 4.2.2

1 - Assign variables city and state to your current city and state of residence. (If residing outside the U.S., substitute the analogous quantities.)

    >> city = "Guarapuava" # My city
    => "Guarapuava"
    >> state = "Paraná" # My state
    => "Paraná"
    >>

2 - Using interpolation, print (using puts) a string consisting of the city and state separated by a comma and as space, as in “Los Angeles, CA”.

    >> puts "#{city}, #{state}"
    Guarapuava, Paraná
    => nil

3 - Repeat the previous exercise but with the city and state separated by a tab character.

    >> puts "#{city}(\t)#{state}"
    Guarapuava(	)Paraná
    => nil

4 - What is the result if you replace double quotes with single quotes in the previous exercise?

    >> puts '#{city}(\t)#{state}'
    #{city}(\t)#{state}
    => nil

#### Section 4.2.3

1 - What is the length of the string “racecar”?

    >> "racecar".length
    => 7

A: 7

2 - Confirm using the reverse method that the string in the previous exercise is the same when its letters are reversed.

    >> "racecar".reverse
    => "racecar"

A: It's the same.

3 - Assign the string “racecar” to the variable s. Confirm using the comparison operator == that s and s.reverse are equal.

    >> s = s.reverse
    => "racecar"

A: True

4 - What is the result of running the code shown in Listing 4.9? How does it change if you reassign the variable s to the string “onomatopoeia”? Hint: Use up-arrow to retrieve and edit previous commands

    >> puts "It's a palindrome!" if s == s.reverse
    It's a palindrome!
    => nil

    >> s = "onomatopoeia"
    => "onomatopoeia"
    >> puts "It's a palindrome!" if s == s.reverse
    => nil

A: In the first test, the setence "It's a palindrome!" is printed in the screen, cause the comparison is true. In the second test the setence isn't printed, cause the comparison is false.

#### Section 4.2.4

1 - By replacing FILL_IN with the appropriate comparison test shown in Listing 4.10, define a method for testing palindromes. Hint: Use the comparison shown in Listing 4.9.

    s == s.reverse

2 - By running your palindrome tester on “racecar” and “onomatopoeia”, confirm that the first is a palindrome and the second isn’t.

A: Ok, confirmed!

3 - By calling the nil? method on palindrome_tester("racecar"), confirm that its return value is nil (i.e., calling nil? on the result of the method should return true). This is because the code in Listing 4.10 prints its responses instead of returning them.

A: Ok.

#### Section 4.3.1

1 - Assign a to be to the result of splitting the string “A man, a plan, a canal, Panama” on comma-space.

    a = “A man, a plan, a canal, Panama”.split(‘,’)


2 - Assign s to the string resulting from joining a on nothing.

    s = a.join

3 - Split s on whitespace and rejoin on nothing. Use the palindrome test from Listing 4.10 to confirm that the resulting string s is not a palindrome by the current definition. Using the downcase method, show that s.downcase is a palindrome.

    s = s.split(“ ”).join.downcase

A: True, it's a palindrome.

4 - What is the result of selecting element 7 from the range of letters a through z? What about the same range reversed? Hint: In both cases you will have to convert the range to an array.

A: G, T

#### Section 4.3.2

1 - Using the range 0..16, print out the first 17 powers of 2.

A: 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32

2 - Define a method called yeller that takes in an array of characters and returns a string with an ALLCAPS version of the input. Verify that yeller([’o’, ’l’, ’d’]) returns "OLD". Hint: Combine map, upcase, and join.

A:

3 - Define a method called random_subdomain that returns a randomly generated string of eight letters.

A:

4 - By replacing the question marks in Listing 4.12 with the appropriate methods, combine split, shuffle, and join to write a function that shuffles the letters in a given string.

A:


#### Section 4.3.3

1 - Define a hash with the keys ’one’, ’two’, and ’three’, and the values ’uno’, ’dos’, and ’tres’. Iterate over the hash, and for each key/value pair print out "’#{key}’ in Spanish is ’#{value}’".

2 - Create three hashes called person1, person2, and person3, with first and last names under the keys :first and :last. Then create a params hash so that params[:father] is person1, params[:mother] is person2, and params[:child] is person3.
Verify that, for example, params[:father][:first] has the right value.

3 - Define a hash with symbol keys corresponding to name, email, and a “password digest”, and values equal to your name, your email address, and a random string of 16 lower-case letters.

4 - Find an online version of the Ruby API and read about the Hash method merge. What is the value of the following expression?

A: {"a"=>100, "b"=>300}


#### Section 4.4.1

1 - What is the literal constructor for the range of integers from 1 to 10?

A: a = Array.new([1,2,3,4,5,6,7,8,9,10])

2 - What is the constructor using the Range class and the new method? Hint: new takes two arguments in this context.

A:

3 - Confirm using the == operator that the literal and named constructors from the previous two exercises are identical.

A: 3


#### Section 4.4.2
1 - What is the class hierarchy for a range? For a hash? For a symbol?

A: Class, Module, Object, BasicObject

2 - Confirm that the method shown in Listing 4.15 works even if we replace self.reverse with just reverse.

A: Ok

#### Section 4.4.3
1 - Verify that “racecar” is a palindrome and “onomatopoeia” is not. What about the name of the South Indian language “Malayalam”? Hint: Downcase it first.

A: True, false, true.

2 - Using Listing 4.16 as a guide, add a shuffle method to the String class. Hint: Refer to Listing 4.12.

A: Ok

3 - Verify that Listing 4.16 works even if you remove self..

A: No

#### Section 4.4.4
1 - By running the Rails console in the toy app’s directory from Chapter 2, confirm that you can create a user object using User.new.

A: Ok

2 - Determine the class hierarchy of the user object.

A: Class, Module, Object, BasicObject

#### Section 4.4.5
1 - In the example User class, change from name to separate first and last name attributes, and then add a method called full_name that returns the first and last names separated by a space. Use it to replace the use of name in the formatted email method.

A: Ok

2 - Add a method called alphabetical_name that returns the last name and first name separated by comma-space.

A: Ok

3 - Verify that full_name.split is the same as alphabetical_name.split(’, ’).reverse.

A: Ok

#### Chapter 5
#### Section 5.1.1
1 - It’s well-known that no web page is complete without a cat image. Using the command in Listing 5.4, arrange to download the kitten pic shown in Figure 5.3.8

    curl -OL cdn.learnenough.com/kitten.jpg

2 - Using the mv command, move kitten.jpg to the correct asset directory for images (Section 5.2.1).

    mv Kitten.jpg app/assets/images

3 - Using image_tag, add kitten.jpg to the Home page, as shown in Figure 5.4.

    File: app/views/static_pages/home.html.erb

    <%= image_tag("Kitten.jpg", alt: "Kitten") %>

#### Section 5.1.2
1 - Using code like that shown in Listing 5.10, comment out the cat image from Section 5.1.1.1. Verify using a web inspector that the HTML for the image no longer appears in the page source.

    File: app/views/static_pages/home.html.erb
    <%#= image_tag("kitten.jpg", alt: "Kitten") %>

2 - By adding the CSS in Listing 5.11 to custom.scss, hide all images in the application—currently just the Rails logo on the Home page). Verify with a web inspector that, although the image doesn’t appear, the HTML source is still present.

    File: app/assets/stylesheets/custom.scss
    img {
      display: none;
    }

#### Section 5.1.3
1 - Replace the default Rails head with the call to render shown in Listing 5.18. Hint: For convenience, cut the default header rather than just deleting it.

A: Ok

2 - Because we haven’t yet created the partial needed by Listing 5.18, the tests should be red. Confirm that this is the case.

A: Yes, the test is RED.

3 - Create the necessary partial in the layouts directory, paste in the contents, and verify that the tests are now green again.

A: Yes, the test is GREEN.

#### Section 5.2.2
1 - As suggested in Section 5.2.2, go through the steps to convert the footer CSS from Listing 5.17 to Listing 5.20 to SCSS by hand.

    /* footer

    footer {
      margin-top: 45px;
      padding-top: 5px;
      border-top: 1px solid #eaeaea;
      color: #777;
      a {
        color: #555;
        &:hover {
          color: #222;
        }
      }
      small {
        float: left;
      }
      ul {
        float: right;
        list-style: none;
        li {
          float: left;
          margin-left: 15px;
        }
      }
    }

#### Section 5.3.2
1 - It’s possible to use a named route other than the default using the as: option. Drawing inspiration from this famous Far Side comic strip, change the route for the Help page to use helf.

    get  '/help',    to: 'static_pages#help', as: 'helf'

2 - Confirm that the tests are now red. Get them to green by updating the route in Listing 5.28.

A: Ok

3 - Revert the changes from these exercises using Undo.

A: Ok

#### Section 5.3.3
1 - Update the layout links to use the helf route from Listing 5.29.

A: Ok

2 - Revert the changes using Undo.

A: Ok

#### Section 5.3.4

1 - In the footer partial, change about_path to contact_path and verify that the tests catch the error.

A: Yes

2 - It’s convenient to use the full_title helper in the tests by including the Application helper into the test helper, as shown in Listing 5.35. We can then test for the right title using code like Listing 5.36. This is brittle, though, because now any typo in the base title (such as “Ruby on Rails Tutoial”) won’t be caught by the test suite. Fix this problem by writing a direct test of the full_title helper, which involves creating a file to test the application helper and then filling in the code indicated with FILL_IN in Listing 5.37. (Listing 5.37 uses assert_equal <expected>, <actual>, which verifies that the expected result matches the actual value when compared with the == operator.)

A: Ok

#### Section 5.4.1
1 - Per Table 5.1, change the route in Listing 5.41 to use signup_path instead of users_new_url.

    get  '/signup',  to: 'users#new'

2 - The route in the previous exercise doesn’t yet exist, so confirm that the tests are now red. (This is intended help us get comfortable with the red/green flow of Test Driven Development (TDD, Box 3.3); we’ll get the tests back to green in Section 5.4.2.)

A: Ok

#### Section 5.4.2
1 - If you didn’t solve the exercise in Section 5.4.1.1, change the test in Listing 5.41 to use the named route signup_path. Because of the route defined in Listing 5.43, this test should initially be green.

A: Ok

2 - In order to verify the correctness of the test in the previous exercise, comment out the signup route to get to red, then uncomment to get to green.

A: Ok

3 - In the integration test from Listing 5.32, add code to visit the signup page using the get method and verify that the resulting page title is correct. Hint: Use the full_title helper as in Listing 5.36.

A: Ok.

#### Chapter 6
#### Section 6.1.1
1 - Rails uses a file called schema.rb in the db/ directory to keep track of the structure of the database (called the schema, hence the filename). Examine your local copy of db/schema.rb and compare its contents to the migration code in Listing 6.2.

A: The contents are similar, but the schema.rb file has some extra fields like  "created_at" and "updated_at".

      ActiveRecord::Schema.define(version: 20170417033519) do
        create_table "users", force: :cascade do |t|
          t.string   "name"
          t.string   "email"
          t.datetime "created_at", null: false
          t.datetime "updated_at", null: false
        end
      end

2 - Most migrations (including all the ones in this tutorial) are reversible, which means we can “migrate down” and undo them with a single command, called db:rollback:

      $ rails db:rollback

After running this command, examine db/schema.rb to confirm that the rollback was successful. (See Box 3.1 for another technique useful for reversing migrations.) Under the hood, this command executes the drop_table command to remove the users table from the database. The reason this works is that the change method knows that drop_table is the inverse of create_table, which means that the rollback migration can be easily inferred. In the case of an irreversible migration, such as one to remove a database column, it is necessary to define separate up and down methods in place of the single change method. Read about migrations in the Rails Guides for more information.

A: Yes, the rollback was successful, the schema.rb file is empty again.

3 - Re-run the migration by executing rails db:migrate again. Confirm that the contents of db/schema.rb have been restored.

A: The contents have been restored.

#### Section 6.1.2
1 - In a Rails console, use the technique from Section 4.4.4 to confirm that User.new is of class User and inherits from ApplicationRecord.

      $ rails c
      Running via Spring preloader in process 12457
      Loading development environment (Rails 5.0.1)
      >> User.new
      => #<User id: nil, name: nil, email: nil, created_at: nil, updated_at: nil>

2 - Confirm that ApplicationRecord inherits from ActiveRecord::Base.

A: Ok

#### Section 6.1.3
1 - Confirm that user.name and user.email are of class String.

    >> user.name
    => "Michael Hartl"
    >> user.email
    => "mhartl@example.com"

A: Yes.

2 - Of what class are the created_at and updated_at attributes?

    >> user.created_at
    => Mon, 17 Apr 2017 03:57:25 UTC +00:00
    >> user.updated_at
    => Mon, 17 Apr 2017 03:57:25 UTC +00:00

A: DateIime

#### Section 6.1.4
1 - Find the user by name. Confirm that find_by_name works as well. (You will often encounter this older style of find_by in legacy Rails applications.)

      >> User.find_by_name("Michael Hartl")
        User Load (0.3ms)  SELECT  "users".* FROM "users" WHERE "users"."name" = ? LIMIT ?  [["name", "Michael Hartl"], ["LIMIT", 1]]
      => nil

A: Yes, it works.

2 - For most practical purposes, User.all acts like an array, but confirm that in fact it’s of class User::ActiveRecord_Relation.

A: Ok

3 - Confirm that you can find the length of User.all by passing it the length method (Section 4.2.3). Ruby’s ability to manipulate objects based on how they act rather than on their formal class type is called duck typing, based on the aphorism that “If it looks like a duck, and it quacks like a duck, it’s probably a duck.”

    >> User.all.length  
      User Load (0.1ms)  SELECT "users".* FROM "users"
    => 0

A: Yes.

#### Section 6.1.5
1 - Update the user’s name using assignment and a call to save.

	>> user.name = "Michael Vornes"
	=> "Michael Vornes"
	>> user.save
	=> true

2 - Update the user’s email address using a call to update_attributes.

	>> user.update_attribute(:name, "michael.vornes@outlook.com")
	=> true
	>> user.email
	=> "michael.vornes@outlook.com"

3 - Confirm that you can change the magic columns directly by updating the created_at column using assignment and a save. Use the value 1.year.ago, which is a Rails way to create a timestamp one year before the present time.

	>> user.created_at = "1.year.ago"
	=> "Mon, 17 Apr 2016 07:57:25 UTC +00:00"
	>> user.save
	=> true

#### Section 6.2.1

1 - In the console, confirm that a new user is currently valid.

    $ rails console --sandbox
  	>> user = User.new(name: "Vornes", email: "vornes@example.com")
  	>> user.valid?
  	=> true

  	>> user = User.new(name: "", email: "vornes@example.com")
  	>> user.valid?
  	=> false

2 - Confirm that the user created in Section 6.1.3 is also valid.

A: Ok

#### Section 6.2.2

1 - Make a new user called u and confirm that it’s initially invalid. What are the full error messages?

	$ rails console --sandbox
	>> u = User.new(name: "Vornes", email: "")
	>> u.valid?
	=> false

	>> u.errors.full_messages
	=> ["Email can't be blank"]


2 - Confirm that u.errors.messages is a hash of errors. How would you access just the email errors?

	>> u.errors.full_messages_for(:email)
	=> ["Email can't be blank"]

#### Section 6.2.3

1 - Make a new user with too-long name and email and confirm that it’s not valid.

	$ rails console --sandbox
	>> user = User.new(name: ""a" * 55", email: ""a" * 260")
	>> user.valid?
	=> false

2 - What are the error messages generated by the length validation?

	>> user.errors.full_messages

### Section 6.2.4

1 - By pasting in the valid addresses from Listing 6.18 and invalid addresses from Listing 6.19 into the test string area at Rubular, confirm that the regex from Listing 6.21 matches all of the valid addresses and none of the invalid ones.

A: Ok

2 - As noted above, the email regex in Listing 6.21 allows invalid email addresses with consecutive dots in the domain name, i.e., addresses of the form foo@bar..com. Add this address to the list of invalid addresses in Listing 6.19 to get a failing test, and then use the more complicated regex shown in Listing 6.23 to get the test to pass.

A: Ok

3 - Add foo@bar..com to the list of addresses at Rubular, and confirm that the regex shown in Listing 6.23 matches all the valid addresses and none of the invalid ones.

A: Ok

#### Section 6.2.5

1 - Add a test for the email downcasing from Listing 6.32, as shown in Listing 6.33. This test uses the reload method for reloading a value from the database and the assert_equal method for testing equality. To verify that Listing 6.33 tests the right thing, comment out the before_save line to get to red, then uncomment it to get to green.

    Arquivo: test/models/user_test.rb

    test "email addresses should be saved as lower-case" do
        mixed_case_email = "Foo@ExAMPle.CoM"
        @user.email = mixed_case_email
        @user.save
        assert_equal mixed_case_email.downcase, @user.reload.email
    end

A: Ok.

2 - By running the test suite, verify that the before_save callback can be written using the “bang” method email.downcase! to modify the email attribute directly, as shown in Listing 6.34.

A: Ok

#### Section 6.3.2

1 - Confirm that a user with valid name and email still isn’t valid overall.

	$ rails console --sandbox
	>> user = User.new(name: "Vornes", email: "vornes@example.com")
	>> user.valid?
	=> false


2 - What are the error messages for a user with no password?

	>> user.errors.full_messages
  => ["Password can't be blank"]

#### Section 6.3.3

1 - Confirm that a user with valid name and email but a too-short password isn’t valid.

	$ rails console --sandbox
	>> user = User.new(name: "Vornes", email: "vornes@example.com", password: "ab", password_confirmation: "ab")
	>> user.valid?
	=> false

2 - What are the associated error messages?

	>> user.errors.full_messages
  ["Password is too short (minimum is 6 characters)"]

#### Section 6.3.4

1 - Quit and restart the console, and then find the user created in this section.

A: Ok

2 - Try changing the name by assigning a new name and calling save. Why didn’t it work?

	>> user.name = "New name"
	=> "New name"
	>> user.save
	=> false

3 - Update user’s name to use your name. Hint: The necessary technique is covered in Section 6.1.5.

	>> user.update_attributes(name: "Michael")
	=> true"
---

#### Section 7.1.1

1 - Visit /about in your browser and use the debug information to determine the controller and action of the params hash.

      --- !ruby/object:ActionController::Parameters
      parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
        controller: static_pages
        action: about
      permitted: false

2 - In the Rails console, pull the first user out of the database and assign it to the variable user. What is the output of puts user.attributes.to_yaml? Compare this to using the y method via y user.attributes.

    >> puts user.attributes.to_yaml
      ---
      id: 1
      name: Vornes
      email: vornes@example.com
      created_at: !ruby/object:ActiveSupport::TimeWithZone
      utc: &1 2017-04-15 18:13:31.274950000 Z
      zone: &2 !ruby/object:ActiveSupport::TimeZone
        name: Etc/UTC
      time: *1
      updated_at: !ruby/object:ActiveSupport::TimeWithZone
      utc: &3 2017-04-15 18:22:21.074011000 Z
      zone: *2
      time: *3

A: With y user.attributes the result is the same.*

#### Section 7.1.2

1 - Using embedded Ruby, add the created_at and updated_at “magic column” attributes to the user show page from Listing 7.4.

        <%= @user.created_at %>, <%= @user.updated_at %>

2 - Using embedded Ruby, add Time.now to the user show page. What happens when you refresh the browser?

        <%= Time.now %>

A: The time is updated.

#### Section 7.1.3

1 - With the debugger in the show action as in Listing 7.6, hit /users/1. Use puts to display the value of the YAML form of the params hash. Hint: Refer to the relevant exercise in Section 7.1.1.1. How does it compare to the debug information shown by the debug method in the site template?

    (byebug) puts @user.attributes.to_yaml

A: Less information than the other method.

2 - Put the debugger in the User new action and hit /users/new. What is the value of @user?

    nill

#### Section 7.1.4

1 - Associate a Gravatar with your primary email address if you haven’t already. What is the MD5 hash associated with the image?


2 - Verify that the code in Listing 7.12 allows the gravatar_for helper defined in Section 7.1.4 to take an optional size parameter, allowing code like gravatar_for user, size: 50 in the view. (We’ll put this improved helper to use in Section 10.3.1.)

A: Ok

3 - The options hash used in the previous exercise is still commonly used, but as of Ruby 2.0 we can use keyword arguments instead. Confirm that the code in Listing 7.13 can be used in place of Listing 7.12. What are the diffs between the two?

A: ok

#### Section 7.2.1

1 - In Listing 7.15, replace :name with :nome. What error message do you get as a result?

      undefined method 'nome' for #<User:0x007f88907e1ba0>
      Did you mean?  name

2 - Confirm by replacing all occurrences of f with foobar that the name of the block variable is irrelevant as far as the result is concerned. Why might foobar nevertheless be a bad choice?

A:

#### Section 7.2.2

1 - Learn Enough HTML to Be Dangerous, in which all HTML is written by hand, doesn’t cover the form tag. Why not?

A:

#### Section 7.3.2

1 - By hitting the URL /users/new?admin=1, confirm that the admin attribute appears in the params debug information.

      --- !ruby/object:ActionController::Parameters
      parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
        admin: '1'
        controller: users
        action: new
      permitted: false

#### Section 7.3.3

1 - Confirm by changing the minimum length of passwords to 5 that the error message updates automatically as well.

A: Yes, update.

2 - How does the URL on the unsubmitted signup form (Figure 7.12) compare to the URL for a submitted signup form (Figure 7.18)? Why don’t they match?

A:

#### Section 7.3.4

1 - Write a test for the error messages implemented in Listing 7.20. How detailed you want to make your tests is up to you; a suggested template appears in Listing 7.25.

A:

2 - The URLs for an unsubmitted signup form and for a submitted signup form are /signup and /users, respectively, which don’t match. This is due to our use of a custom named route in the former case (added in Listing 5.43) and a default RESTful route in the latter case (Listing 7.3). Resolve this discrepancy by adding the code shown in Listing 7.26 and Listing 7.27. Submit the new form to confirm that both cases now use the same /signup URL. Are the tests still green? Why?

A:

3 - Update the post in Listing 7.25 to use the new URL from the previous exercise. Confirm that the tests are still green.

A:

4 - Confirm by reverting Listing 7.27 to its previous form (Listing 7.20) that the tests are still green. This is a problem, because the URL being posted to isn’t right. Add an assert_select to the test in Listing 7.25 to catch this bug and get to red, then change the form back to Listing 7.27 to get the tests green again. Hint: Test for the presence of ’form[action="/signup"]’ before posting to the form in the test.

A:

#### Section 7.4.1

1 - Using the Rails console, verify that a user is in fact created when submitting valid information.

A: Yes, user is created.

2 - Confirm by updating Listing 7.28 and submitting a valid user that redirect_to user_url(@user) has the same effect as redirect_to @user.

A: Yes, the same effect.

#### Section 7.4.2

1 - In the console, confirm that you can use interpolation (Section 4.2.2) to interpolate a raw symbol. For example, what is the return value of "#{:success}"?

      => "success"

2 - How does the previous exercise relate to the flash iteration shown in Listing 7.30?

A:

#### Section 7.4.3

1 - Using the Rails console, find by the email address to double-check that the new user was actually created. The result should look something like Listing 7.32.

      >> User.find_by(email: "michael.vornes@outlook.com")
        User Load (0.2ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "michael.vornes@outlook.com"], ["LIMIT", 1]]
      => #<User id: 2, name: "Michael Vornes", email: "michael.vornes@outlook.com", created_at: "2017-04-25 02:48:55", updated_at: "2017-04-25 02:48:55", password_digest: "$2a$10$vO6fe7.o4uTU0i0dGt1beOPDbj/oZroYS4V4R5q07rm...">
      >>

2 - Create a new user with your primary email address. Verify that the Gravatar correctly appears.


    AboutContactNews--- !ruby/object:ActionController::Parameters
    parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
      controller: users
      action: show
      id: '2'
    permitted: false

A: Ok

#### Section 7.4.4

1 - Write a test for the flash implemented in Section 7.4.2. How detailed you want to make your tests is up to you; a suggested ultra-minimalist template appears in Listing 7.34, which you should complete by replacing FILL_IN with the appropriate code. (Even testing for the right key, much less the text, is likely to be brittle, so I prefer to test only that the flash isn’t empty.)

A:

2 - As noted above, the flash HTML in Listing 7.31 is ugly. Verify by running the test suite that the cleaner code in Listing 7.35, which uses the Rails content_tag helper, also works.

A:

3 - Verify that the test fails if you comment out the redirect line in Listing 7.28.

A:

4 - Suppose we changed @user.save to false in Listing 7.28. How does this change verify that the assert_difference block is testing the right thing?

A:
.

#### Section 8.1.1

1 - What is the difference between GET login_path and POST login_path?

A: GET login_path opens login form and POST login_path receive the data login send by the users via GET login_path

2 - By piping the results of rails routes to grep, list all the routes associated with the Users resource. Do the same for Sessions. How many routes does each resource have? Hint: Refer to the section on grep in Learn Enough Command Line to Be Dangerous.

      rails routes | grep users
         signup GET    /signup(.:format)         users#new
                POST   /signup(.:format)         users#create
          users GET    /users(.:format)          users#index
                POST   /users(.:format)          users#create
       new_user GET    /users/new(.:format)      users#new
      edit_user GET    /users/:id/edit(.:format) users#edit
           user GET    /users/:id(.:format)      users#show
                PATCH  /users/:id(.:format)      users#update
                PUT    /users/:id(.:format)      users#update
                DELETE /users/:id(.:format)      users#destroy

#### Section 8.1.2
1 - Submissions from the form defined in Listing 8.4 will be routed to the Session controller’s create action. How does Rails know to do this? Hint: Refer to Table 8.1 and the first line of Listing 8.5.

A: The form sends a POST request for login with "post '/login' declaration, to: 'sessions#create'" on routes.rb file that notice the controller and the correct function thar should be used.

#### Section 8.1.3
1 - Using the Rails console, confirm each of the values in Table 8.2. Start with user = nil, and then use user = User.first. Hint: To coerce the result to a boolean value, use the bang-bang trick from Section 4.2.3, as in !!(user && user.authenticate(’foobar’)).

A:
    !!(nil && User.first.authenticate("abc")) -> false
    !!(User.first && User.first.authenticate("nopass")) -> false
    !!(User.first && User.first.authenticate("abc")) - true

#### Section 8.1.5
1 - Verify in your browser that the sequence from Section 8.1.4 works correctly, i.e., that the flash message disappears when you click on a second page.

A: Ok.

#### Section 8.2.1
1 - Log in with a valid user and inspect your browser’s cookies. What is the value of the session content? Hint: If you don’t know how to view your browser’s cookies, Google for it (Box 1.1).

A:

2 - What is the value of the Expires attribute from the previous exercise?

A:

#### Section 8.2.2
1 - Confirm at the console that User.find_by(id: ...) returns nil when the corresponding user doesn’t exist.

    User.find_by(id: 1) -> <User id: 1, name: "Rails Tutorial", email: "example@railstutorial.org"

2 - In a Rails console, create a session hash with key :user_id. By following the steps in Listing 8.17, confirm that the ||= operator works as required.

    \@current_user ||= User.find_by(id: session[:user_id]) -> nil
    current_user ||= User.find_by(id: session[:user_id])

A: the command loads user correpondent to id 1, then if the command run again, the user continues loaded as well.

#### Section 8.2.3
1 - Using the cookie inspector in your browser (Section 8.2.1.1), remove the session cookie and confirm that the layout links revert to the non-logged-in state.

A: Ok.

2 - Log in again, confirming that the layout links change correctly. Then quit your browser and start it again to confirm that the layout links revert to the non-logged-in state. (If your browser has a “remember where I left off” feature that automatically restores the session, be sure to disable it in this step (Box 1.1).)

A: Ok.

#### Section 8.2.4
1 - Delete the bang ! in the Session helper’s logged_in? method and confirm that the test in Listing 8.23 is red.

A: Ok

2 - Restore the ! to get back to green.

A: Ok

#### Section 8.2.5
1 - Is the test suite red or green if you comment out the log_in line in Listing 8.25?

A: Red

2 - By using your text editor’s ability to comment out code, toggle back and forth between commenting out code in Listing 8.25 and confirm that the test suite toggles between red and green. (You will need to save the file between toggles.)

A: Ok.

#### Section 8.3
1 - Confirm in a browser that the “Log out” link causes the correct changes in the site layout. What is the correspondence between these changes and the final three steps in Listing 8.31?

A: When you click on "Log out" the session user_id is erased and the conditional expression at the header shows the login link only, so the assert_select verify that the links are true.

2 - By checking the site cookies, confirm that the session is correctly removed after logging out.

A: Ok.

#### Section 9.1.1
1 - In the console, assign user to the first user in the database, and verify by calling it directly that the remember method works. How do remember_token and remember_digest compare?

      >> u = User.first
        User Load (0.2ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<User id: 1, name: "Rails Tutorial", email: "example@tutorilas.org", created_at: "2017-04-26 02:45:56", updated_at: "2017-04-26 02:45:56", password_digest: "$2a$10$0Xkj3kDS0NHLY94Knp126eZUXyTLPCbHNc55rdFZvG3...", remember_digest: nil>
      >> u.remember
         (0.1ms)  begin transaction
        SQL (0.4ms)  UPDATE "users" SET "updated_at" = ?, "remember_digest" = ? WHERE "users"."id" = ?  [["updated_at", 2017-05-09 22:56:27 UTC], ["remember_digest", "$2a$10$BDbYcM7I/uBACBwaCAzxtu7ew1xbLzD35uirBod9skKs85OnhMZ5e"], ["id", 1]]
         (98.6ms)  commit transaction
      => true
      >> u.remember_token
      => "KkpXkGx2GuAOnWTctYLOiw"
      >> u.remember_digest
      => "$2a$10$BDbYcM7I/uBACBwaCAzxtu7ew1xbLzD35uirBod9skKs85OnhMZ5e"
      >>


2 - In Listing 9.3, we defined the new token and digest class methods by explicitly prefixing them with User. This works fine and, because they are actually called using User.new_token and User.digest, it is probably the clearest way to define them. But there are two perhaps more idiomatically correct ways to define class methods, one slightly confusing and one extremely confusing. By running the test suite, verify that the implementations in Listing 9.4 (slightly confusing) and Listing 9.5 (extremely confusing) are correct. (Note that, in the context of Listing 9.4 and Listing 9.5, self is the User class, whereas the other uses of self in the User model refer to a user object instance. This is part of what makes them confusing.)

A: Ok

#### Section 9.1.2
1 - By finding the cookie in your local browser, verify that a remember token and encrypted user id are present after logging in.

A:

2 - At the console, verify directly that the authenticated? method defined in Listing 9.6 works correctly.

      >> u = User.first
        User Load (0.2ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<User id: 1, name: "Rails Tutorial", email: "example@tutorilas.org", created_at: "2017-04-26 02:45:56", updated_at: "2017-05-09 23:13:06", password_digest: "$2a$10$0Xkj3kDS0NHLY94Knp126eZUXyTLPCbHNc55rdFZvG3...", remember_digest: "$2a$10$C4WLd6hY8e6QEwuXRmEfe.LAzJMfYX8AMRK9B7FKHIn...">
      >> u.remember
         (0.1ms)  begin transaction
        SQL (1.2ms)  UPDATE "users" SET "updated_at" = ?, "remember_digest" = ? WHERE "users"."id" = ?  [["updated_at", 2017-05-09 23:13:47 UTC], ["remember_digest", "$2a$10$zJczKW32adHctuFx7vKsZenAGpNO9f5Dg/d/APQD6rnmVh2p6UNVe"], ["id", 1]]
         (99.9ms)  commit transaction
      => true
      >>
      ?> t = u.remember_token
      => "-naDl-mBJKNabtcG9hboCg"
      >> u.authenticated? t
      => true
      >>

#### Section 9.1.3
After logging out, verify that the corresponding cookies have been removed from your browser.

A: Yes, only the sample_app_session apears

#### Section 9.1.4
1 - Comment out the fix in Listing 9.16 and then verify that the first subtle bug is present by opening two logged-in tabs, logging out in one, and then clicking “Log out” link in the other.

      undefined method 'forget' for nil:NilClass

2 - Comment out the fix in Listing 9.19 and verify that the second subtle bug is present by logging out in one browser and closing and opening the second browser.

      invalid hash

3 - Uncomment the fixes and confirm that the test suite goes from red to green

A: Ok

#### Section 9.2

1 - By inspecting your browser’s cookies directly, verify that the “remember me” checkbox is having its intended effect.

A: user_id and remember_token only appears if remember me was checked on login page.

2 - At the console, invent examples showing both possible behaviors of the ternary operator (Box 9.2).

      >> x = true
      => true
      >> t = x ? "verdadeiro" : "falso"
      => "verdadeiro"
      >> puts t
      verdadeiro
      => nil
      >> x = false
      => false
      >> t = x ? "verdadeiro" : "falso"
      => "falso"
      >> puts t
      falso
      => nil

#### Section 9.3.1

1 - As mentioned above, the application currently doesn’t have any way to access the virtual remember_token attribute in the integration test in Listing 9.25. It is possible, though, using a special test method called assigns. Inside a test, you can access instance variables defined in the controller by using assigns with the corresponding symbol. For example, if the create action defines an @user variable, we can access it in the test using assigns(:user). Right now, the Sessions controller create action defines a normal (non-instance) variable called user, but if we change it to an instance variable we can test that cookies correctly contains the user’s remember token. By filling in the missing elements in Listing 9.27 and Listing 9.28 (indicated with question marks ? and FILL_IN), complete this improved test of the “remember me” checkbox.

A: Ok.

#### Section 9.3.2

1 - Verify by removing the authenticated? expression in Listing 9.33 that the second test in Listing 9.31 fails, thereby confirming that it tests the right thing.

A: Ok

## Chapter 10
#### Section 10.1.1

1 - As noted above, there’s a minor security issue associated with using target="_ blank" to open URLs, which is that the target site gains control of what’s known as the “window object” associated with the HTML document. The result is that the target site could potentially introduce malicious content, such as a phishing page. This is extremely unlikely to happen when linking to a reputable site like Gravatar, but it turns out that we can eliminate the risk entirely by setting the rel attribute (“relationship”) to "noopener" in the origin link. Add this attribute to the Gravatar edit link in Listing 10.2.

A: Ok

2 - Remove the duplicated form code by refactoring the new.html.erb and edit.html.erb views to use the partial in Listing 10.5, as shown in Listing 10.6 and Listing 10.7. Note the use of the provide method, which we used before in Section 3.4.3 to eliminate duplication in the layout.3 (If you solved the exercise corresponding to Listing 7.27, this exercise won’t work exactly as written, and you’ll have to apply your technical sophistication to resolve the discrepancy. My suggestion is to use the variable-passing technique shown in Listing 10.5 to pass the needed URL from Listing 10.6 and Listing 10.7 to the form in Listing 10.5.)

A: Ok

#### Section 10.1.2

1 - Confirm by submitting various invalid combinations of username, email, and password that the edit form won’t accept invalid submissions.

A: All the validations are available on the edit form.

#### Section 10.1.3

1 - Add a line in Listing 10.9 to test for the correct number of error messages. Hint: Use an assert_select (Table 5.2) that tests for a div with class alert containing the text “The form contains 4 errors.”

A: Ok

#### Section 10.1.4

1 - Double-check that you can now make edits by making a few changes on the development version of the application.

A: Ok

2 - What happens when you change the email address to one without an associated Gravatar?

A: The default gravatar image appears

#### Section 10.2.1

1 - As noted above, by default before filters apply to every action in a controller, which in our cases is an error (requiring, e.g., that users log in to hit the signup page, which is absurd). By commenting out the only: hash in Listing 10.15, confirm that the test suite catches this error.

      29 tests, 70 assertions, 2 failures, 0 errors, 0 skips

#### Section 10.2.2
1 - Why is it important to protect both the edit and update actions?

A: It is important because visiting users (not logged) or others users should not visualize the edit form or change the others users information

2 - Which action could you more easily test in a browser?

A: Edit action

#### Section 10.2.3

1 - Write a test to make sure that friendly forwarding only forwards to the given URL the first time. On subsequent login attempts, the forwarding URL should revert to the default (i.e., the profile page). Hint: Add to the test in Listing 10.29 by checking for the right value of session[:forwarding_url].

A: Ok

2 - Put a debugger (Section 7.1.3) in the Sessions controller’s new action, then log out and try to visit /users/1/edit. Confirm in the debugger that the value of session[:forwarding_url] is correct. What is the value of request.get? for the new action? (Sometimes the terminal can freeze up or act strangely when you’re using the debugger; use your technical sophistication (Box 1.1) to resolve any issues.)

    (byebug) session[:forwarding_url] = "http://localhost:3000//users/1/edit"
    request.get? = true

#### Section 10.3.1

1 - We’ve now filled in all the links in the site layout. Write an integration test for all the layout links, including the proper behavior for logged-in and non-logged-in users. Hint: Use the log_in_as helper and add to the steps shown in Listing 5.32 .

A: Ok

#### Section 10.3.2

1 - Verify that trying to visit the edit page of another user results in a redirect as required by Section 10.2.2.

A: Ok

#### Section 10.3.3

1 - Confirm at the console that setting the page to nil pulls out the first page of users.

A: Yes

2 - What is the Ruby class of the pagination object? How does it compare to the class of User.all?

    >> User.all.class
    => User::ActiveRecord_Relation
    >> User.paginate(page: nil).class
    => User::ActiveRecord_Relation
    >>

A: The both methods returns User::ActiveRecord_Relation object

#### Section 10.3.4

1 - By commenting out the pagination links in Listing 10.45, confirm that the test in Listing 10.48 goes red.

A: Yes

2 - Confirm that commenting out only one of the calls to will_paginate leaves the tests green. How would you test for the presence of both sets of will_paginate links? Hint: Use a count from Table 5.2.

      Expected exactly 2 elements matching "div.pagination", found 1

#### Section 10.3.5

1 - Comment out the render line in Listing 10.52 and confirm that the resulting tests are red.

A: Yes

#### Section 10.4.1

1 - By issuing a PATCH request directly to the user path as shown in Listing 10.56, verify that the admin attribute isn’t editable through the web. To be sure your test is covering the right thing, your first step should be to add admin to the list of permitted parameters in user_params so that the initial test is red.

A:Ok

#### Section 10.4.2

1 - As the admin user, destroy a few sample users through the web interface. What are the corresponding entries in the server log?

      (0.4ms)  begin transaction
      SQL (2.5ms)  DELETE FROM "users" WHERE "users"."id" = ?  [["id", 10]]
      (18.9ms)  commit transaction

#### Section 10.4.3

1 - By commenting out the admin user before filter in Listing 10.59, confirm that the tests go red.

A: Yes

## Chapter 11
#### Section 11.1.1

1 - Verify that the test suite is still green.

      Finished in 2.33708s
      34 tests, 142 assertions, 0 failures, 0 errors, 0 skips

2 - Why does Table 11.2 list the _ url form of the named route instead of the _ path form? Hint: We’re going to use it in an email.

A: Because _ path returns only '/account_activation/<token>/edit' and _ url returns 'exampledomain.com/account_activation/<token>/edit'

#### Section 11.1.2

1 - Verify that the test suite is still green after the changes made in this section.

      Finished in 2.27979s
      34 tests, 142 assertions, 0 failures, 0 errors, 0 skips

2 - By instantiating a User object in the console, confirm that calling the create_activation_digest method raises a NoMethodError due to its being a private method. What is the value of the user’s activation digest?

      >> u.create_activation_digest
      => "$2a$10$R3thbETHMwhPcppHlznwyejlGoiHLyTWfyjrbKeE53FdJVzFm5Qfu"
      >> NoMethodError: private method 'create_activation_digest' called for #<User:0x0000003715c078>
      Did you mean?  restore_activation_digest!
      >> u.activation_digest
      => "$2a$10$R3thbETHMwhPcppHlznwyejlGoiHLyTWfyjrbKeE53FdJVzFm5Qfu"


3 - In Listing 6.34, we saw that email downcasing can be written more simply as email.downcase! (without any assignment). Make this change to the downcase_email method in Listing 11.3 and verify by running the test suite that it works.  

      def downcase_email
        email.downcase!
      end

      Finished in 2.29536s
      34 tests, 142 assertions, 0 failures, 0 errors, 0 skips

#### Section 11.2.1

1 - At the console, verify that the escape method in the CGI module escapes out the email address as shown in Listing 11.15. What is the escaped value of the string "Don’t panic!"?

      Loading development environment (Rails 5.0.1)
      >> CGI.escape('foo@example.com')
      => "foo%40example.com"
      >> CGI.escape("Don't panic!")
      => "Don%27t+panic%21"
      >>

#### Section 11.2.2

1 - Preview the email templates in your browser. What do the Date fields read for your previews?

      Date: Wed, 17 May 2017 23:26:13 +0000

#### Section 11.2.3

1 - Verify that the full test suite is still green.

      Finished in 2.69304s
      35 tests, 151 assertions, 0 failures, 0 errors, 0 skips

2 - Confirm that the test goes red if you remove the call to CGI.escape in Listing 11.20.

      FAIL["test_account_activation", UserMailerTest, 4.272937458001252]
      test_account_activation#UserMailerTest (4.27s)
      Expected /michael@example\.com/ to match # encoding: US-ASCII

#### Section 11.2.4

1 - Sign up as a new user and verify that you’re properly redirected. What is the content of the generated email in the server log? What is the value of the activation token?

      UserMailer#account_activation: processed outbound mail in 16.2ms
      Sent mail to mvornes@outlook.com (9.7ms)
      Date: Wed, 17 May 2017 23:30:41 -0300
      From: noreply@example.com
      To: mvornes@outlook.com
      Message-ID: <591d075115e1e_17ee172e1289952@vornes-HP-G42-Notebook-PC.mail>
      Subject: Account activation
      Mime-Version: 1.0
      Content-Type: multipart/alternative;
       boundary="--==_ mimepart_591d0751144e5_17ee172e1289944f";
       charset=UTF-8
      Content-Transfer-Encoding: 7bit


      ----==_mimepart_591d0751144e5_17ee172e1289944f
      Content-Type: text/plain;
       charset=UTF-8
      Content-Transfer-Encoding: 7bit

      Hi Michael Vornes,

      Welcome to the Sample App! Click on the link below to activate your account:

      https://localhost:3000/account_activations/Kx8hnNikyXYxWflnHSlkPw/edit?email=mvornes%40outlook.com


      ----==_mimepart_591d0751144e5_17ee172e1289944f
      Content-Type: text/html;
       charset=UTF-8
      Content-Transfer-Encoding: 7bit

      <!DOCTYPE html>
      <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <style>
            /* Email styles need to be inline */
          </style>
        </head>

        <body>
          <h1>Sample App</h1>

      <p>Hi Michael Vornes,</p>

      <p>
      Welcome to the Sample App! Click on the link below to activate your account:
      </p>

      <a href="https://localhost:3000/account_activations/Kx8hnNikyXYxWflnHSlkPw/edit?email=mvornes%40outlook.com">Activate</a>

        </body>
      </html>

      ----==_ mimepart_591d0751144e5_17ee172e1289944f--

      Redirected to http://localhost:3000/
      Completed 302 Found in 405ms (ActiveRecord: 137.2ms)

A: Activaton token: Kx8hnNikyXYxWflnHSlkPw

2 - Verify at the console that the new user has been created but that it is not yet activated.

      $ rails c
      Running via Spring preloader in process 4186
      Loading development environment (Rails 5.0.1)
      >> User.find_by(email: 'mvornes@outlook.com').activated?
        User Load (0.3ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "mvornes@outlook.com"], ["LIMIT", 1]]
      => false

#### Section 11.3.1

1 - Create and remember new user at the console. What are the user’s remember and activation tokens? What are the corresponding digests?

      >> u = User.create(name: "User", email: "user@example.com", password: "abc123", password_confirmation: "abc123")
         (0.1ms)  begin transaction
        User Exists (0.3ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) LIMIT ?  [["email", "user@example.com"], ["LIMIT", 1]]
        SQL (0.4ms)  INSERT INTO "users" ("name", "email", "created_at", "updated_at", "password_digest", "activation_digest") VALUES (?, ?, ?, ?, ?, ?)  [["name", "User"], ["email", "user@example.com"], ["created_at", 2017-05-18 03:30:31 UTC], ["updated_at", 2017-05-18 03:30:31 UTC], ["password_digest", "$2a$10$rQ2K8q/4GtCS2k1DrWNFTe2ufiTG2kumteaYotv4iUHXs5e6MuqS2"], ["activation_digest", "$2a$10$INhtgDibhB/2pH1174mVcOqp6GWGeNoPHZalry9DTFdr1i2hLII/S"]]
         (155.1ms)  commit transaction
      => #<User id: 104, name: "User", email: "user@example.com", created_at: "2017-05-18 03:30:31", updated_at: "2017-05-18 03:30:31", password_digest: "$2a$10$rQ2K8q/4GtCS2k1DrWNFTe2ufiTG2kumteaYotv4iUH...", remember_digest: nil, admin: false, activation_digest: "$2a$10$INhtgDibhB/2pH1174mVcOqp6GWGeNoPHZalry9DTFd...", activated: false, activated_at: nil>

      >> u.remember
         (0.1ms)  begin transaction
        SQL (1.2ms)  UPDATE "users" SET "updated_at" = ?, "remember_digest" = ? WHERE "users"."id" = ?  [["updated_at", 2017-05-18 03:30:47 UTC], ["remember_digest", "$2a$10$UxuajDtm4QEe4nj1wI5CV.SswCC9.Xsjuj9yii/RyyhmTs43py7SK"], ["id", 104]]
         (84.1ms)  commit transaction
      => true

      >> u.remember_token
      => "7MlvoqFg3i2cIcasB-BQHg"

      >> u.activation_token
      => "XlovbJWJQUa9RsRTiI8Q8g"

      >> u.remember_digest
      => "$2a$10$UxuajDtm4QEe4nj1wI5CV.SswCC9.Xsjuj9yii/RyyhmTs43py7SK"

      >> u.activation_digest
      => "$2a$10$INhtgDibhB/2pH1174mVcOqp6GWGeNoPHZalry9DTFdr1i2hLII/S"
      >>

2 - Using the generalized authenticated? method from Listing 11.26, verify that the user is authenticated according to both the remember token and the activation token.

      >> u.authenticated?(:remember, u.remember_token)
      => true
      >> u.authenticated?(:activation, u.activation_token)
      => true
      >>

#### Section 11.3.2

1 - Paste in the URL from the email generated in Section 11.2.4. What is the activation token?

      >> u.activation_token
      => "XlovbJWJQUa9RsRTiI8Q8g"

2 - Verify at the console that the User is authenticated according to the activation token in the URL from the previous exercise. Is the user now activated?

      >> u.reload.activated?
      => true

#### Section 11.3.3

1 - In Listing 11.35, the activate method makes two calls to the update_attribute, each of which requires a separate database transaction. By filling in the template shown in Listing 11.39, replace the two update_attribute calls with a single call to update_columns, which hits the database only once. After making the changes, verify that the test suite is still green.

      Finished in 2.67450s
      35 tests, 159 assertions, 0 failures, 0 errors, 0 skips

A: Ok

2 - Right now all users are displayed on the user index page at /users and are visible via the URL /users/:id, but it makes sense to show users only if they are activated. Arrange for this behavior by filling in the template shown in Listing 11.40.9 (This uses the Active Record where method, which we’ll learn more about in Section 13.3.3.)

A: Ok

3 - To test the code in the previous exercise, write integration tests for both /users and /users/:id.

A: Ok

## Chapter 12
#### Section 12.1.1

1 - Verify that the test suite is still green.

      Finished in 18.62348s
      35 tests, 159 assertions, 0 failures, 0 errors, 0 skips

2 - Why does Table 12.1 list the _ url form of the edit named route instead of the _ path form? Hint: The answer is the same as for the similar account activations exercise (Section 11.1.1.1).

A: Because _ url is the complete path, that consider hostname, and the _ path is only the relative path.

#### Section 12.1.2

1 - Why does the form_for in Listing 12.4 use :password_reset instead of @password_reset?

A: Because @password_reset refers to model.

#### Section 12.1.3

1 - Submit a valid email address to the form shown in Figure 12.6. What error message do you get?

Browser:
      Email address not found

            --- !ruby/object:ActionController::Parameters
      parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
        utf8: "✓"
        authenticity_token: 4s8xh+ET+G1LE1aov+pzyTQZaI1ibBrh//5BVcWHwk20va6dx+tiCjuGZSISgtW/X7pCX9w0MbWQ3ymkgFUZ9w==
        password_reset: !ruby/object:ActionController::Parameters
          parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
            email: fake@email.com
          permitted: false
        commit: Submit
        controller: password_resets
        action: create
      permitted: false

2 - Confirm at the console that the user in the previous exercise has valid reset_digest and reset_sent_at attributes, despite the error. What are the attribute values?

A: Ok

#### Section 12.2.1

1 - Preview the email templates in your browser. What do the Date fields read for your previews?

      Date: Thu, 18 May 2017 13:01:49 +0000

2 - Submit a valid email address to the new password reset form. What is the content of the generated email in the server log?

      UserMailer#password_reset: processed outbound mail in 33.7ms
      Sent mail to mvornes@outlook.com (20.2ms)
      Date: Thu, 18 May 2017 10:05:26 -0300
      From: noreply@example.com
      To: mvornes@outlook.com
      Message-ID: <591d9c168dad3_f0c1603618883c4@vornes-HP-G42-Notebook-PC.mail>
      Subject: Password reset
      Mime-Version: 1.0
      Content-Type: multipart/alternative;
       boundary="--==_mimepart_591d9c16893f3_f0c16036188822d";
       charset=UTF-8
      Content-Transfer-Encoding: 7bit


      ----==_mimepart_591d9c16893f3_f0c16036188822d
      Content-Type: text/plain;
       charset=UTF-8
      Content-Transfer-Encoding: 7bit

      To reset your password click the link below:

      https://localhost:3000/password_resets/dAscE0ahLerRTf2O5v4w2Q/edit?email=mvornes%40outlook.com

      This link will expire in two hours.

      If you did not request your password to be reset, please ignore this email and
      your password will stay as it is.


      ----==_mimepart_591d9c16893f3_f0c16036188822d
      Content-Type: text/html;
       charset=UTF-8
      Content-Transfer-Encoding: 7bit

      <!DOCTYPE html>
      <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <style>
            /* Email styles need to be inline */
          </style>
        </head>

        <body>
          <h1>Password reset</h1>

      <p>To reset your password click the link below:</p>

      <a href="https://localhost:3000/password_resets/dAscE0ahLerRTf2O5v4w2Q/edit?email=mvornes%40outlook.com">Reset password</a>

      <p>This link will expire in two hours.</p>

      <p>
      If you did not request your password to be reset, please ignore this email and
      your password will stay as it is.
      </p>

        </body>
      </html>

      ----==_ mimepart_591d9c16893f3_f0c16036188822d--

      Redirected to http://localhost:3000/
      Completed 302 Found in 467ms (ActiveRecord: 303.8ms)

3 - At the console, find the user object corresponding to the email address from the previous exercise and verify that it has valid reset_digest and reset_sent_at attributes.

      >> u = User.find_by(email: "mvornes@outlook.com")
        User Load (0.3ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "mvornes@outlook.com"], ["LIMIT", 1]]
      => #<User id: 102, name: "Michael Vornes", email: "mvornes@outlook.com", created_at: "2017-05-18 02:30:40", updated_at: "2017-05-18 13:05:26", password_digest: "$2a$10$Hsip7cAaiD.3agSUOAWtIebKKnGYXMTI1mqpTcs7XG/...", remember_digest: nil, admin: false, activation_digest: "$2a$10$OnnMh1/eSR.B/eWAcIBY..RP2ZYdS/C/EIArrHT9tZk...", activated: true, activated_at: "2017-05-18 03:43:25", reset_digest: "$2a$10$wRZAs9PeXhDhSf27pkfUpe1tQhwhOjD8XlXOEwQpdxJ...", reset_sent_at: "2017-05-18 13:05:26">
      >> u.reset_digest
      => "$2a$10$wRZAs9PeXhDhSf27pkfUpe1tQhwhOjD8XlXOEwQpdxJLDU7SWcOMO"
      >> u.reset_sent_at
      => Thu, 18 May 2017 13:05:26 UTC +00:00
      >>

#### Section 12.2.2

1 - Run just the mailer tests. Are they green?

      $ rails test:mailers
      /home/vornes/.rvm/gems/ruby-2.4.0@web5/gems/activesupport-5.0.1/lib/active_support/xml_mini.rb:51: warning: constant ::Fixnum is deprecated
      /home/vornes/.rvm/gems/ruby-2.4.0@web5/gems/activesupport-5.0.1/lib/active_support/xml_mini.rb:52: warning: constant ::Bignum is deprecated
      /home/vornes/.rvm/gems/ruby-2.4.0@web5/gems/activesupport-5.0.1/lib/active_support/core_ext/numeric/conversions.rb:138: warning: constant ::Fixnum is deprecated
      Started with run options --seed 49187

      /home/vornes/.rvm/gems/ruby-2.4.0@web5/gems/activejob-5.0.1/lib/active_job/arguments.rb:38: warning: constant ::Fixnum is deprecated
      /home/vornes/.rvm/gems/ruby-2.4.0@web5/gems/activejob-5.0.1/lib/active_job/arguments.rb:38: warning: constant ::Bignum is deprecated
        2/2: [===================================] 100% Time: 00:00:00, Time: 00:00:00

      Finished in 0.76080s
      2 tests, 16 assertions, 0 failures, 0 errors, 0 skips


2 - Confirm that the test goes red if you remove the second call to CGI.escape in Listing 12.12.

A: Ok

#### Section 12.3.1

1 - Follow the link in the email from the server log in Section 12.2.1.1. Does it properly render the form as shown in Figure 12.11?

A: Yes

2 - What happens if you submit the form from the previous exercise?

A: Error

    Error: The action 'update' could not be found for PasswordResetsController

#### Section 12.3.2

1 - Follow the email link from Section 12.2.1.1 again and submit mismatched passwords to the form. What is the error message?

      Password confirmation doesn't match Password

2 - In the console, find the user belonging to the email link, and retrieve the value of the password_digest attribute. Now submit valid matching passwords to the form shown in Figure 12.12. Did the submission appear to work? How did it affect the value of password_digest? Hint: Use user.reload to retrieve the new value.

      >> u = User.find_by(email: "mvornes@outlook.com")
        User Load (0.4ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "mvornes@outlook.com"], ["LIMIT", 1]]
      => #<User id: 102, name: "Michael Vornes", email: "mvornes@outlook.com", created_at: "2017-05-18 02:30:40", updated_at: "2017-05-18 13:05:26", password_digest: "$2a$10$Hsip7cAaiD.3agSUOAWtIebKKnGYXMTI1mqpTcs7XG/...", remember_digest: nil, admin: false, activation_digest: "$2a$10$OnnMh1/eSR.B/eWAcIBY..RP2ZYdS/C/EIArrHT9tZk...", activated: true, activated_at: "2017-05-18 03:43:25", reset_digest: "$2a$10$wRZAs9PeXhDhSf27pkfUpe1tQhwhOjD8XlXOEwQpdxJ...", reset_sent_at: "2017-05-18 13:05:26">
      >> u.password_digest
      => "$2a$10$Hsip7cAaiD.3agSUOAWtIebKKnGYXMTI1mqpTcs7XG/dZNvDpk6wG"
      >> u.reload.password_digest
        User Load (0.6ms)  SELECT  "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 102], ["LIMIT", 1]]
      => "$2a$10$Hsip7cAaiD.3agSUOAWtIebKKnGYXMTI1mqpTcs7XG/dZNvDpk6wG"
      >>

A: Yes it works.

#### Section 12.3.3

1 - In Listing 12.6, the create_reset_digest method makes two calls to update_attribute, each of which requires a separate database operation. By filling in the template shown in Listing 12.20, replace the two update_attribute calls with a single call to update_columns, which hits the database only once. After making the changes, verify that the test suite is still green. (For convenience, Listing 12.20 includes the results of solving the exercise in Listing 11.39.)

A: Ok

2 - Write an integration test for the expired password reset branch in Listing 12.16 by filling in the template shown in Listing 12.21. (This code introduces response.body, which returns the full HTML body of the page.) There are many ways to test for the result of an expiration, but the method suggested by Listing 12.21 is to (case-insensitively) check that the response body includes the word “expired”.

A: Ok

3 - Expiring password resets after a couple of hours is a nice security precaution, but there is an even more secure solution for cases where a public computer is used. The reason is that the password reset link remains active for 2 hours and can be used even if logged out. If a user reset their password from a public machine, anyone could press the back button and change the password (and get logged in to the site). To fix this, add the code shown in Listing 12.22 to clear the reset digest on successful password update.4

A: Ok

4 - Add a line to Listing 12.18 to test for the clearing of the reset digest in the previous exercise. Hint: Combine assert_nil (first seen in Listing 9.25) with user.reload (Listing 11.33) to test the reset_digest attribute directly.

A: Ok

#### Section 12.4

1 - Sign up for a new account in production. Did you get the email?

A: Yes

2 - Click on the link in the activation email to confirm that it works. What is the corresponding entry in the server log? Hint: Run heroku logs at the command line.

      2017-05-18T16:48:18.672638+00:00 heroku[router]: at=info method=GET path="/account_activations/R--lLFzuklc5RS8SwvPLcQ/edit?email=mvornes%40outlook.com" host=sampleapp-vornes.herokuapp.com request_id=7375fa9c-2b28-46d8-9775-ca538e3bde37 fwd="191.218.68.77" dyno=web.1 connect=1ms service=7ms status=302 bytes=1043 protocol=https
      2017-05-18T16:48:18.663292+00:00 app[web.1]: I, [2017-05-18T16:48:18.663198 #4]  INFO -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37] Started GET "/account_activations/R--lLFzuklc5RS8SwvPLcQ/edit?email=mvornes%40outlook.com" for 191.218.68.77 at 2017-05-18 16:48:18 +0000
      2017-05-18T16:48:18.664391+00:00 app[web.1]: I, [2017-05-18T16:48:18.664325 #4]  INFO -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37] Processing by AccountActivationsController#edit as HTML
      2017-05-18T16:48:18.664472+00:00 app[web.1]: I, [2017-05-18T16:48:18.664407 #4]  INFO -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37]   Parameters: {"email"=>"mvornes@outlook.com", "id"=>"R--lLFzuklc5RS8SwvPLcQ"}
      2017-05-18T16:48:18.667546+00:00 app[web.1]: D, [2017-05-18T16:48:18.667472 #4] DEBUG -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37]   User Load (1.0ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = $1 LIMIT $2  [["email", "mvornes@outlook.com"], ["LIMIT", 1]]
      2017-05-18T16:48:18.668309+00:00 app[web.1]: I, [2017-05-18T16:48:18.668233 #4]  INFO -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37] Redirected to https://sampleapp-vornes.herokuapp.com/
      2017-05-18T16:48:18.668536+00:00 app[web.1]: I, [2017-05-18T16:48:18.668462 #4]  INFO -- : [7375fa9c-2b28-46d8-9775-ca538e3bde37] Completed 302 Found in 4ms (ActiveRecord: 1.0ms)

3 - Are you able to successfully update your password?

A: Yes

## Chapter 13
#### Section 13.1.1
1 - Using Micropost.new in the console, instantiate a new Micropost object called micropost with content “Lorem ipsum” and user id equal to the id of the first user in the database. What are the values of the magic columns created_at and updated_at?

      >> m = Micropost.new(user: User.first, content: 'Lorem ipsum')
        User Load (0.3ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<Micropost id: nil, content: "Lorem ipsum", user_id: 1, created_at: nil, updated_at: nil>
      >> m.created_at
      => nil
      >> m.updated_at
      => nil
      >>

2 - What is micropost.user for the micropost in the previous exercise? What about micropost.user.name?

    >> m.user
    => #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-05-17 19:41:09", updated_at: "2017-05-17 19:41:09", password_digest: "$2a$10$jRJm/txCToo5fArggMI2UuqMEEG7aSJaovcNv0OdTKh...", remember_digest: nil, admin: true, activation_digest: "$2a$10$d1DMoQuT0ygyUooJPFmK7.dmyU7A6/FSiIbGe3iln23...", activated: true, activated_at: "2017-05-17 19:41:09", reset_digest: nil, reset_sent_at: nil>
    >> m.user.name
    => "Example User"
    >>

3 - Save the micropost to the database. What are the values of the magic columns now?

    >> m.save!
       (0.1ms)  begin transaction
      SQL (1.6ms)  INSERT INTO "microposts" ("content", "user_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["content", "Lorem ipsum"], ["user_id", 1], ["created_at", 2017-06-05 19:34:34 UTC], ["updated_at", 2017-06-05 19:34:34 UTC]]
       (129.3ms)  commit transaction
    => true
    >> m.created_at
    => Mon, 05 Jun 2017 19:34:34 UTC +00:00
    >> m.updated_at
    => Mon, 05 Jun 2017 19:34:34 UTC +00:00
    >>

#### Section 13.1.2

1 - At the console, instantiate a micropost with no user id and blank content. Is it valid? What are the full error messages?

      >> m = Micropost.new
      => #<Micropost id: nil, content: nil, user_id: nil, created_at: nil, updated_at: nil>
      >> m.valid?
      => false

      >> m.errors
      => #<ActiveModel::Errors:0x00000003a94d88 @base=#<Micropost id: nil, content: nil, user_id: nil, created_at: nil, updated_at: nil>, @messages={:user=>["must exist"], :user_id=>["can't be blank"], :content=>["can't be blank"]}, @details={:user=>[{:error=>:blank}], :user_id=>[{:error=>:blank}], :content=>[{:error=>:blank}]}>
      >>

2 - At the console, instantiate a second micropost with no user id and content that’s too long. Is it valid? What are the full error messages?

      >> m = Micropost.new(content: "a" * 141)
      => #<Micropost id: nil, content: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...", user_id: nil, created_at: nil, updated_at: nil>
      >> m.valid?
      => false

      >> m.errors
      => #<ActiveModel::Errors:0x00000004ce3610 @base=#<Micropost id: nil, content: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...", user_id: nil, created_at: nil, updated_at: nil>, @messages={:user=>["must exist"], :user_id=>["can't be blank"], :content=>["is too long (maximum is 140 characters)"]}, @details={:user=>[{:error=>:blank}], :user_id=>[{:error=>:blank}], :content=>[{:error=>:too_long, :count=>140}]}>
      >>

#### Section 13.1.3

1 - Set user to the first user in the database. What happens when you execute the command micropost = user.microposts.create(content: "Lorem ipsum")?

      >> user = User.first
        User Load (0.3ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-05-17 19:41:09", updated_at: "2017-05-17 19:41:09", password_digest: "$2a$10$jRJm/txCToo5fArggMI2UuqMEEG7aSJaovcNv0OdTKh...", remember_digest: nil, admin: true, activation_digest: "$2a$10$d1DMoQuT0ygyUooJPFmK7.dmyU7A6/FSiIbGe3iln23...", activated: true, activated_at: "2017-05-17 19:41:09", reset_digest: nil, reset_sent_at: nil>
      >> micropost = user.microposts.create(content: "Lorem ipsum")
         (0.1ms)  begin transaction
        SQL (14.6ms)  INSERT INTO "microposts" ("content", "user_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["content", "Lorem ipsum"], ["user_id", 1], ["created_at", 2017-06-05 20:12:47 UTC], ["updated_at", 2017-06-05 20:12:47 UTC]]
         (106.7ms)  commit transaction
      => #<Micropost id: 2, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 20:12:47", updated_at: "2017-06-05 20:12:47">
      >>

A: The command creates a micropost on database with content "Lorem ipsum" and user_id with the id of the first user.

2 - The previous exercise should have created a micropost in the database. Confirm this by running user.microposts.find(micropost.id). What if you write micropost instead of micropost.id?

      >> user.microposts.find(micropost.id)
        Micropost Load (0.3ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? AND "microposts"."id" = ? LIMIT ?  [["user_id", 1], ["id", 2], ["LIMIT", 1]]
      => #<Micropost id: 2, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 20:12:47", updated_at: "2017-06-05 20:12:47">
      >>

      > user.microposts.find(micropost)
      DEPRECATION WARNING: You are passing an instance of ActiveRecord::Base to `find`. Please pass the id of the object by calling `.id`. (called from irb_binding at (irb):4)
        Micropost Load (0.1ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? AND "microposts"."id" = ? LIMIT ?  [["user_id", 1], ["id", 2], ["LIMIT", 1]]
      => #<Micropost id: 2, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 20:12:47", updated_at: "2017-06-05 20:12:47">

3 - What is the value of user == micropost.user? How about user.microposts.first == micropost?

    >> user == micropost.user
    => true

    >> user.microposts.first == micropost
      Micropost Load (0.3ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? ORDER BY "microposts"."id" ASC LIMIT ?  [["user_id", 1], ["LIMIT", 1]]
    => false

A: false cause this is the second micropost

#### Section 13.1.4

1 - How does the value of Micropost.first.created_at compare to Micropost.last.created_at?

      >> Micropost.first.created_at
        Micropost Load (24.7ms)  SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" DESC LIMIT ?  [["LIMIT", 1]]
      => Mon, 05 Jun 2017 20:19:34 UTC +00:00

      >> Micropost.last.created_at
        Micropost Load (0.3ms)  SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" ASC LIMIT ?  [["LIMIT", 1]]
      => Mon, 05 Jun 2017 19:34:34 UTC +00:00
      >>

A: Micropost.first.created_at returns the last post created, but the first in the desc order. Micropost.last.created_at returns the firt post created, but the last post in the desc order.

2 - What are the SQL queries for Micropost.first and Micropost.last? Hint: They are printed out by the console.

    >> Micropost.first
      Micropost Load (0.2ms)  SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" DESC LIMIT ?  [["LIMIT", 1]]
    => #<Micropost id: 3, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 20:19:34", updated_at: "2017-06-05 20:19:34">
    >> Micropost.last
      Micropost Load (0.2ms)  SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" ASC LIMIT ?  [["LIMIT", 1]]
    => #<Micropost id: 1, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 19:34:34", updated_at: "2017-06-05 19:34:34">
    >>

3 - Let user be the first user in the database. What is the id of its first micropost? Destroy the first user in the database using the destroy method, then confirm using Micropost.find that the user’s first micropost was also destroyed.

      >> user = User.first
        User Load (0.3ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-05-17 19:41:09", updated_at: "2017-05-17 19:41:09", password_digest: "$2a$10$jRJm/txCToo5fArggMI2UuqMEEG7aSJaovcNv0OdTKh...", remember_digest: nil, admin: true, activation_digest: "$2a$10$d1DMoQuT0ygyUooJPFmK7.dmyU7A6/FSiIbGe3iln23...", activated: true, activated_at: "2017-05-17 19:41:09", reset_digest: nil, reset_sent_at: nil>

      >> user.microposts.first.id
        Micropost Load (0.3ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? ORDER BY "microposts"."created_at" DESC LIMIT ?  [["user_id", 1], ["LIMIT", 1]]
      => 3

      >> user.microposts.first.destroy
        Micropost Load (0.2ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? ORDER BY "microposts"."created_at" DESC LIMIT ?  [["user_id", 1], ["LIMIT", 1]]
         (0.1ms)  begin transaction
        SQL (1.3ms)  DELETE FROM "microposts" WHERE "microposts"."id" = ?  [["id", 3]]
         (91.2ms)  commit transaction
      => #<Micropost id: 3, content: "Lorem ipsum", user_id: 1, created_at: "2017-06-05 20:19:34", updated_at: "2017-06-05 20:19:34">

      >> Micropost.find 3
        Micropost Load (0.3ms)  SELECT  "microposts".* FROM "microposts" WHERE "microposts"."id" = ? ORDER BY "microposts"."created_at" DESC LIMIT ?  [["id", 3], ["LIMIT", 1]]
      ActiveRecord::RecordNotFound: Couldn't find Micropost with 'id'=3

#### Section 13.2.1

1 - As mentioned briefly in Section 7.3.3, helper methods like time_ago_in_words are available in the Rails console via the helper object. Using helper, apply time_ago_in_words to 3.weeks.ago and 6.months.ago.

      >> helper.time_ago_in_words 3.weeks.ago
      => "21 days"
      >> helper.time_ago_in_words 6.months.ago
      => "6 months"
      >>

2 - What is the result of helper.time_ago_in_words(1.year.ago)?

      >> helper.time_ago_in_words 1.year.ago
      => "about 1 year"
      >>

3 - What is the Ruby class for a page of microposts? Hint: Use the code in Listing 13.23 as your model, and call the class method on paginate with the argument page: nil.

      >> User.first.microposts.paginate(page: nil).class
        User Load (0.2ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => Micropost::ActiveRecord_AssociationRelation

#### Section 13.2.2

1 - See if you can guess the result of running (1..10).to_a.take(6). Check at the console to see if your guess is right.

      >> (1..10).to_a.take(6)
      => [1, 2, 3, 4, 5, 6]
      >>

2 - Is the to_a method in the previous exercise necessary?

      > (1..10).take(6)
      => [1, 2, 3, 4, 5, 6]
      >>

A: No, it is not necessary.

3 - Faker has a huge number of occasionally amusing applications. By consulting the Faker documentation, learn how to print out a fake university name, a fake phone number, a fake Hipster Ipsum sentence, and a fake Chuck Norris fact.

      >> Faker::University.name
      => "East Indiana Academy"
      >> Faker::PhoneNumber.phone_number
      => "460-680-3607"
      >> Faker::Hipster.sentence
      => "Beard +1 meh kombucha scenester."
      >> Faker::ChuckNorris.fact
      => "Chuck Norris' protocol design method has no status, requests or responses, only commands."
      >> Faker::ChuckNorris.fact
      => "Quantum cryptography does not work on Chuck Norris. When something is being observed by Chuck it stays in the same state until he's finished. "
      >> Faker::ChuckNorris.fact
      => "Chuck Norris hosting is 101% uptime guaranteed."
      >>

#### Section 13.2.3

1 - Comment out the application code needed to change the two ’h1’ lines in Listing 13.28 from green to red.

A: Ok

3 - Update Listing 13.28 to test that will_paginate appears only once. Hint: Refer to Table 5.2.

A: Ok

#### Section 13.3.1
1 - Why is it a bad idea to leave a copy of logged_in_user in the Users controller?

A: Code redundancy. The User controller function can override the Application controller's function.

#### Section 13.3.2

Refactor the Home page to use separate partials for the two branches of the if-else statement.

A: Ok

#### Section 13.3.3

1 - Use the newly created micropost UI to create the first real micropost. What are the contents of the INSERT command in the server log?

INSERT INTO "microposts" ("content", "user_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["content", "Second real post"], ["user_id", 1], ["created_at", 2017-06-06 16:38:16 UTC], ["updated_at", 2017-06-06 16:38:16 UTC]]

2 - In the console, set user to the first user in the database. Confirm that the values of by Micropost.where("user_id = ?", user.id), user.microposts, and user.feed are all the same. Hint: It’s probably easiest to compare directly using ==.

      >> user = User.first
        User Load (0.2ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
      => #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-06-06 01:25:19", updated_at: "2017-06-06 01:25:19", password_digest: "$2a$10$mGFnGRudc5zydf7OGmRyMODh19RXSzXbTvykYaOc6D0...", remember_digest: nil, admin: true, activation_digest: "$2a$10$B6qF/NYDlSVsr60C0psNGu5y7BLbAjFBea7OFyZMJCM...", activated: true, activated_at: "2017-06-06 01:25:19", reset_digest: nil, reset_sent_at: nil>

      >> Micropost.where("user_id = ?", user.id) == user.microposts
        Micropost Load (0.6ms)  SELECT "microposts".* FROM "microposts" WHERE "microposts"."user_id" = ? ORDER BY "microposts"."created_at" DESC  [["user_id", 1]]
        Micropost Load (0.7ms)  SELECT "microposts".* FROM "microposts" WHERE (user_id = 1) ORDER BY "microposts"."created_at" DESC
      => true

      >> user.microposts == user.feed
        Micropost Load (0.7ms)  SELECT "microposts".* FROM "microposts" WHERE (user_id = 1) ORDER BY "microposts"."created_at" DESC
      => true

      >> Micropost.where("user_id = ?", user.id) == user.feed
      => true
      >>

#### Section 13.3.4

1 - Create a new micropost and then delete it. What are the contents of the DELETE command in the server log?

      DELETE FROM "microposts" WHERE "microposts"."id" = ?  [["id", 303]]

2 - Confirm directly in the browser that the line redirect_to request.referrer || root_url can be replaced with the line redirect_back(fallback_location: root_url). (This method was added in Rails 5.)

A: Ok

#### Section 13.3.5

1 - For each of the four scenarios indicated by comments in Listing 13.55 (starting with “Invalid submission”), comment out application code to get the corresponding test to red, then uncomment to get back to green.

A: Ok

2 - Add tests for the sidebar micropost count (including proper pluralization). Listing 13.57 will help get you started.

A: Ok

#### Section 13.4.1

1 - Upload a micropost with attached image. Does the result look too big? (If so, don’t worry; we’ll fix it in Section 13.4.3).

A: Yes.

2 - Following the template in Listing 13.63, write a test of the image uploader in Section 13.4. As preparation, you should add an image to the fixtures directory (using, e.g, cp app/assets/images/rails.png test/fixtures/). The additional assertions in Listing 13.63 check both for a file upload field on the Home page and for a valid image attribute on the micropost resulting from valid submission. Note the use of the special fixture_file_upload method for uploading files as fixtures inside tests.18 Hint: To check for a valid picture attribute, use the assigns method mentioned in Section 11.3.3 to access the micropost in the create action after valid submission.

A: Ok

#### Section 13.4.2

What happens if you try uploading an image bigger than 5 megabytes?

A: Browser shows up the alert "Maximum file size is 5MB. Please choose a smaller file."

What happens if you try uploading a file with an invalid extension?

A: It is not allowed to choose other files that are not images

#### Section 13.4.3

1 - Upload a large image and confirm directly that the resizing is working. Does the resizing work even if the image isn’t square?

A: Yes, it works.

2 - If you completed the image upload test in Listing 13.63, at this point your test suite may be giving you a confusing error message. Fix this issue by configuring CarrierWave to skip image resizing in tests using the initializer file shown in Listing 13.68.

A: Ok.

#### Section 13.4.4

1 - Upload a large image and confirm directly that the resizing is working in production. Does the resizing work even if the image isn’t square?

A: Ok.
